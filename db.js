const mongoose = require('mongoose');

const mongoDB = process.env.MONGODB_URI;

module.exports = mongoose.connect(mongoDB);