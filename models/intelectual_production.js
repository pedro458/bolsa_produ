const mongoose = require('mongoose');

const TableEntries = require('./table_entries');

const FieldsSchema = new mongoose.Schema({
    entry: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'TableEntries',
        required: true
    },
    productions: {
        p2015: {
            type: Number
        },
        p2016: {
            type: Number
        },
        p2017: {
            type: Number
        },
        p2018: {
            type: Number
        },
        required: true
    },
    total: {
     type: Number,
     required: true   
    }
})

const Field = mongoose.model('Field', FieldsSchema)

module.exports = Field;