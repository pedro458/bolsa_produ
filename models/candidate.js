const mongoose = require('mongoose');require('mongoose-type-email');

const Attachment = require('./attachment');
const Program = require('./program');
const Knowledge = require('./knowledge');
const Field = require('./intelectual_production');


const CandidateSchema = new mongoose.Schema({
    personal_informations: {
        name: {
            type: String,
            required: true
        },
        cpf: {
            type: String,
            required: true
        },
        primary_email: {
            type: mongoose.SchemaTypes.Email,
            required: true
        },
        secondary_email: {
            type: mongoose.SchemaTypes.Email
        },
        phone: {
            type: String
        },
        address: {
            type: String
        }
    },
    area: {
        knowledge: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Knowledge',
            require: true
        },
        research_group: {
            type: String,
            require: true
        },
        programA: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Program',
            required: true
        },
        programB: {
            type: mongoose.SchemaType.ObjectId,
            ref: 'Program',
        }
    },
    intelectual_production: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Field'
    }],
    attachment: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Attachment',
        required: true
    }
})

const Candidate = mongoose.model('Candidate', CandidateSchema);

module.exports = Candidate;