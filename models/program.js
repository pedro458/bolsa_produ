const mongoose = require('mongoose');

const ProgramSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

const Program = mongoose.Model('Program', ProgramSchema);

module.exports = Program;