const mongoose = require('mongoose');

const TableEntriesSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }, 
    points: {
        type: Number,
        require: true
    },
    max: {
        type: Number,
        required: true
    }
});

const TableEntries = mongoose.model('TableEntries', TableEntriesSchema);

module.exports = TableEntries;