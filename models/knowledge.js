const mongoose = require('mongoose');

const KnowledgeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

const Knowledge = mongoose.model('Knowledge', KnowledgeSchema)

module.exports = Knowledge