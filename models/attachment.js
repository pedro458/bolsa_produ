const mongoose = require('mongoose');

const AttachmentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    }
})

const Attachment = mongoose.model('Attachment', FileSchema);

module.exports = Attachment;